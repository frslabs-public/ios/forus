#
# Be sure to run `pod lib lint Torus.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Forus'
  s.version          = '1.0.0'
  s.summary          = 'Forus is used for Face detection'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/forus'
  s.license          = 'MIT'
  s.author           = { 'nagendra' => 'nagendra@frslabs.com' }
  s.source           = { :http => 'https://forus-ios.repo.frslabs.space/forus-ios/1.0.0/FaceSDKFramework.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '11.0'
  s.ios.vendored_frameworks = 'FaceSDKFramework.framework'
  s.swift_version = '5.0'
end
