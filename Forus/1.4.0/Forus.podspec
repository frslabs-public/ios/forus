

Pod::Spec.new do |s|
  s.name             = 'Forus'
  s.version          = '1.4.0'
  s.summary          = 'Cropus is used to capture and crop signature'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/forus'
  s.license          = 'MIT'
  s.author           = {'sravani' => 'shravani@frslabs.com' }

s.source           = {:http => 'https://forus-ios.repo.frslabs.space/forus-ios/1.4.0/Forus.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '11.0'
  s.ios.vendored_frameworks = 'Forus.framework'
  s.swift_version = '5.0'
end
