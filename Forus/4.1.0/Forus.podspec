

Pod::Spec.new do |s|
  s.name             = 'Forus'
  s.version          = '4.1.0'
  s.summary          = 'Face detection and verification process'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/forus'
  s.license          = 'MIT'
  s.author           = {'sravani' => 'shravani@frslabs.com' }

s.source           = {:http => 'https://forus-ios.repo.frslabs.space/forus-ios/4.1.0/Forus.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '12.0'
  s.ios.vendored_frameworks = 'Forus.framework'
  s.swift_version = '5.0'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
end
